package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// 画面遷移するときの処理
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// 不正チェックフラグ
		boolean Injustice = false;
		List<Branch> branches = new BranchService().getBranch();
		List<Position> positions = new PositionService().getPosition();
		User editUser = null;
		HttpSession session = request.getSession();

		try {
			int user_id = Integer.parseInt(request.getParameter("user_id"));
			editUser = new UserService().getUser(user_id);

			if (editUser == null) {
				Injustice = true;
			}
		} catch (NumberFormatException e) {
			Injustice = true;
		}
		if (Injustice) {
			List<String> messages = new ArrayList<String>();
			messages.add("不正なパラメータです。");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
		} else {
			// editUserに代入
			request.setAttribute("position", positions);
			request.setAttribute("branch", branches);
			request.setAttribute("editUser", editUser);
			// settingに移動
			request.getRequestDispatcher("setting.jsp").forward(request, response);
		}

	}

	// 入力した後の確認の部分
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		// 入力した内容読み込み
		User editUser = getEditUser(request);

		if (isValid(request, messages) == true) {
			try {//
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("setting.jsp").forward(request, response);
				return;
			}

			// session.setAttribute("User", editUser);
			response.sendRedirect("management");
		} else {
			List<Branch> branches = new BranchService().getBranch();
			List<Position> positions = new PositionService().getPosition();
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.setAttribute("position", positions);
			request.setAttribute("branch", branches);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
		}
	}

	// 入力した内容を反映
	private User getEditUser(HttpServletRequest request) throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setLogin_id(request.getParameter("login_id"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
		editUser.setPosition_id(Integer.parseInt(request.getParameter("position_id")));

		return editUser;
	}

	// 入力した内容を確認
	private boolean isValid(HttpServletRequest request, List<String> messages) throws IOException, ServletException {
		User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
		String name = request.getParameter("name");
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String confirm_password = request.getParameter("confirm_password");
		User user = new UserService().getUser(login_id);

		if (StringUtils.isBlank(login_id) == true) {
			messages.add("ログインIDを入力してください");
		} else if (login_id.length() < 6 || login_id.length() > 20) {
			messages.add("ログインIDは6文字以上20文字以下で入力してください");
		} else if (!login_id.matches("[0-9a-zA-Z_]+")) {
			messages.add("ログインIDは半角英数字で入力してください");
		} else if (user != null && !login_id.equals(editUser.getLogin_id())) {
			messages.add("既にこのログインIDは使われています");
		}

		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		} else if (name.length() > 10) {
			messages.add("名前は10文字以下で入力してください");
		}

		if (!StringUtils.isBlank(password) && password.length() < 6 || password.length() > 20) {
			messages.add("パスワードは6文字以上20文字以下で入力してください");
		} else if (!StringUtils.isEmpty(password) && !password.matches("[ -~｡-ﾟ]+")) {
			messages.add("パスワードは半角文字で入力してください");
		} else if (!password.equals(confirm_password)) {
			messages.add("パスワードが一致しません");
		}

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}

	}

}