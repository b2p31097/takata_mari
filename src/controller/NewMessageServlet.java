package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/newmessage" })
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("newmessage.jsp").forward(request, response);

	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		User user = (User) session.getAttribute("loginUser");
		Message message = new Message();
		message.setText(request.getParameter("text"));

		message.setTitle(request.getParameter("title"));
		message.setCategory(request.getParameter("category"));
		message.setUser_id(user.getId());

		if (isValid(request, messages) == true) {
			new MessageService().register(message);
			response.sendRedirect("./");
		}else{
			session.setAttribute("errorMessages", messages);
			request.setAttribute("message",message);
			request.getRequestDispatcher("newmessage.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String text = request.getParameter("text");


		if (StringUtils.isBlank(title) == true) {
			messages.add("件名を入力してください");
		}else if (30< title.length()) {
				messages.add("件名が30文字を超えています");
		}
		if (StringUtils.isBlank(text) == true) {
			messages.add("本文を入力してください");
		}else if (1000 < text.length()) {
				messages.add("1000文字以下で入力してください");
		}
		if (StringUtils.isBlank(category) == true) {
			messages.add("カテゴリーを入力してください");
		}else if (10< category.length()) {
			messages.add("カテゴリーが10文字を超えています");
		}


		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}