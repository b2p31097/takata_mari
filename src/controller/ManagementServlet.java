package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import beans.Users;
import service.StopService;
import service.UserService;
import service.UsersService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	boolean Injustice = false;
  
    	HttpSession session = request.getSession();
    	User user = (User) session.getAttribute("loginUser");
        List<Users> Users = new UsersService().getUsers();
        request.setAttribute("Users", Users);
    	
        request.getRequestDispatcher("management.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

			User user = new UserService().getUser(Integer.parseInt(request.getParameter("user_id")));
			if(user.getIs_stopped()==1){
				new StopService().stop(user);
			}else{
				new StopService().revive(user);
			}
			response.sendRedirect("management");
		}
}
