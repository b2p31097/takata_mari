package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		List<Branch> branches = new BranchService().getBranch();
		List<Position> positions = new PositionService().getPosition();
		request.setAttribute("position", positions);
		request.setAttribute("branch", branches);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User user = new User();

		user.setName(request.getParameter("name"));
		user.setLogin_id(request.getParameter("login_id"));
		user.setPassword(request.getParameter("password"));
		user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
		user.setPosition_id(Integer.parseInt(request.getParameter("position_id")));

		if (isValid(request, messages) == true) {
			new UserService().register(user);
			response.sendRedirect("management");
		}else{
			List<Branch> branches = new BranchService().getBranch();
			List<Position> positions = new PositionService().getPosition();
			request.setAttribute("position", positions);
			request.setAttribute("branch", branches);
			session.setAttribute("errorMessages", messages);
			request.setAttribute("User", user);
			System.out.println(user.getBranch_id());
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String confirm_password = request.getParameter("confirm_password");
		User user = new UserService().getUser(login_id);

		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}else if (name.length()>10) {
			messages.add("名前は10文字以下で入力してください");
		}
		if (StringUtils.isBlank(login_id) == true) {
			messages.add("ログインIDを入力してください");
		}else if (login_id.length()<6||login_id.length()>20) {
			messages.add("ログインIDは6文字以上20文字以下で入力してください");
		}else if (!login_id.matches("[0-9a-zA-Z_]+")) {
			messages.add("ログインIDは半角英数字で入力してください");
		}else if (user!=null) {
			messages.add("既にこのログインIDは使われています");
		}
		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		}else if (password.length()<6||password.length()>20) {
			messages.add("パスワードは6文字以上20文字以下で入力してください");
		}else if (!password.matches("[ -~｡-ﾟ]+")) {
			messages.add("パスワードは半角文字で入力してください");
		}
		if (StringUtils.isBlank(confirm_password) == true) {
			messages.add("確認用パスワードを入力してください");
		}else if (!password .equals(confirm_password)) {
			messages.add("パスワードが一致しません");
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}