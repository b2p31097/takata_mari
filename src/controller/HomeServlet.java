
package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.Message;
import beans.Search;
import beans.User;
import beans.UserMessage;
import service.CommentService;
import service.DeleteService;
import service.MessageService;
import service.SearchService;

@WebServlet(urlPatterns = { "/index.jsp"})
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		//ホーム画面に遷移、絞込みを場合分け
		HttpSession session = request.getSession();
		List<UserMessage> message = new MessageService().getMessage();
		request.setAttribute("messages", message);
		List<Comment> comments = new CommentService().getComment();
		request.setAttribute("comments", comments);

		if (StringUtils.isEmpty(request.getParameter("category"))&&StringUtils.isEmpty(request.getParameter("started_date"))&&
				StringUtils.isEmpty(request.getParameter("closed_date"))){

			request.getRequestDispatcher("home.jsp").forward(request, response);
			//response.sendRedirect("./");
			return;
		}

		Search search= new Search();
		Search item= new Search();
		List<String> messages = new ArrayList<String>();

		//絞込み
		//カテゴリーが入ってる場合

		if (!StringUtils.isEmpty(request.getParameter("category"))) {
			search.setCategory(request.getParameter("category"));
			item.setCategory(request.getParameter("category"));
		}
			//開始が入ってなかったら
			if(request.getParameter("started_date").length()==0){
				search.setStart_date("(NOW()-INTERVAL 1 YEAR) ");
			}else if(!request.getParameter("started_date").matches("^\\d{4}/\\d{2}/\\d{2}$")){
				messages.add("入力した値が不正です");
				session.setAttribute("errorMessages", messages);
				request.getRequestDispatcher("home.jsp").forward(request, response);
				return;
			}else{
				search.setStart_date(request.getParameter("started_date")+" 00:00:00");
				item.setStart_date(request.getParameter("started_date"));
			}
			//終了が入ってなかったら
			if(request.getParameter("closed_date").length()==0){
				search.setClose_date("NOW()");
			}else if(!request.getParameter("closed_date").matches("^\\d{4}/\\d{2}/\\d{2}$")){
				messages.add("入力した値が不正です");
				session.setAttribute("errorMessages", messages);
				request.getRequestDispatcher("home.jsp").forward(request, response);
				return;

			}else{
				search.setClose_date(request.getParameter("closed_date")+" 23:59:59");
				item.setClose_date(request.getParameter("closed_date"));
			}
			//検索結果をリストに追加
			List<Search> searchlist = new SearchService().getSearch(search);

			//リストをmessagesに代入
			request.setAttribute("messages", searchlist);
			//searchの中身を保持
			request.setAttribute("search",item);

			//検索結果が見つからない場合
			if(searchlist.isEmpty()){

				messages.add("この条件では見つかりませんでした");
				//投稿一覧取得、リストに追加

				request.setAttribute("messages", message);
				session.setAttribute("errorMessages", messages);
				request.getRequestDispatcher("home.jsp").forward(request, response);
				return;
			}

			//homeへ
			request.getRequestDispatcher("home.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		//コメント機能
		//messageがあったら
		if (request.getParameter("comment")!=null) {
			HttpSession session = request.getSession();
			List<String> messages = new ArrayList<String>();
			User user = (User) session.getAttribute("loginUser");
			Comment comment = new Comment();
			comment.setText(request.getParameter("comment"));
			comment.setMessage_id(Integer.parseInt(request.getParameter("message")));
			comment.setUser_id(user.getId());


			if (isValid(request, messages) == true) {

				new CommentService().register(comment);
				response.sendRedirect("./");
			}else{
				request.setAttribute("comment", comment);
				session.setAttribute("commenterrorMessages", messages);
				List<UserMessage> message = new MessageService().getMessage();
				request.setAttribute("messages", message);
				List<Comment> comments = new CommentService().getComment();
				request.setAttribute("comments", comments);

				request.getRequestDispatcher("home.jsp").forward(request, response);
			}
		}
		Message message = new Message();
		Comment comment= new Comment();
		//削除機能
		//comment_idかmessage_idがあったら
		if (request.getParameter("comment_id")!=null) {
			comment.setId(Integer.parseInt(request.getParameter("comment_id")));
			new DeleteService().entry(comment);
			response.sendRedirect("./");
		}else if(request.getParameter("message_id")!=null){
			message.setId(Integer.parseInt(request.getParameter("message_id")));
			new DeleteService().register(message);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String comment = request.getParameter("comment");

		if (StringUtils.isBlank(comment) == true) {
			messages.add("コメントを入力してください");
		}
		if (500 < comment.length()) {
			messages.add("500文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		}else{
			return false;
		}
	}


}
