package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		HttpSession session = request.getSession();
		User user = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");
		if (user != null) {
			session.removeAttribute("loginUser");
			// セッションの無効化
		}
		request.getRequestDispatcher("login.jsp").forward(request, response);
		//response.sendRedirect("login.jsp");
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// 入力した値をString型に代入
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		LoginService loginService = new LoginService();
		// ユーザー情報を持ってくる
		User user = loginService.login(login_id, password);
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		// もしユーザーが空っぽじゃなかったら（ユーザー情報が見つかったら）
		if (user != null) {
			// 上書き
			session.setAttribute("loginUser", user);
			// ページを移動
			response.sendRedirect("./");
		} else {
			messages.add("ログインに失敗しました。");
			session.setAttribute("errorMessages", messages);
			request.setAttribute("loginUser", login_id);
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
	}
}