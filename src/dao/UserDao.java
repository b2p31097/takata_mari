package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	public  void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			//signup
			//SQL文作成
			//usersにカラムとデータを登録
			//カラム
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", password");
			sql.append(", position_id");
			sql.append(", is_stopped");
			sql.append(", created_date");
			sql.append(", update_date");
			//中のデータ（これから入力するから？にしておく）
			sql.append(") VALUES (");
			sql.append("?"); // login_id
			sql.append(", ?"); // name
			sql.append(", ?"); // branch_id
			sql.append(", ?"); // password
			sql.append(", ?"); // position_id
			sql.append(", 1"); // is_stopped
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // update_date
			sql.append(")");

			//これから実行するもの入れるよ
			ps = connection.prepareStatement(sql.toString());

			//1つめの？にはこの値を入れるよ
			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranch_id());
			ps.setString(4, user.getPassword());
			ps.setInt(5, user.getPosition_id());
			//ひとつのSQL文になる

			//SQLで実行処理
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public User getUser(Connection connection, String login_id,
			String password) {
		//login
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ?  AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public User getUser(Connection connection, String login_id) {
		//idだけ取得
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<User> toUserList(ResultSet rs) throws SQLException {
		//リストにユーザー情報を追加
		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				int branch_id = rs.getInt("branch_id");
				String password = rs.getString("password");
				int position_id = rs.getInt("position_id");
				int is_stopped = rs.getInt("is_stopped");
				Timestamp created_date = rs.getTimestamp("created_date");
				Timestamp updateDate = rs.getTimestamp("update_date");

				User user = new User();
				user.setId(id);
				user.setLogin_id(login_id);
				user.setName(name);
				user.setBranch_id(branch_id);
				user.setPassword(password);
				user.setPosition_id(position_id);
				user.setIs_stopped(is_stopped);
				user.setCreated_date(created_date);
				user.setUpdate_date(updateDate);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	public User getUser(Connection connection, int id) {
		//idだけ取得
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  login_id = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			if (StringUtils.isBlank(user.getPassword()) != true ){
			sql.append(", password = ?");
			}
			sql.append(", position_id = ?");

			sql.append(", update_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranch_id());
			if (StringUtils.isBlank(user.getPassword()) != true ){
			ps.setString(4, user.getPassword());
			ps.setInt(5, user.getPosition_id());
			ps.setInt(6, user.getId());
			}else{
				ps.setInt(4, user.getPosition_id());
				ps.setInt(5, user.getId());
			}
			
			
			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
	public List<User> getUsers(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.branch_id as branch_id, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.position_id as position_id, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM users ");

            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


	}
