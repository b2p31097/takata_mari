<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ログイン</title>
</head>
<body>
	<div class="main-contents">
		<div class="profile">
			<div class="name">
				<h2>
					社内掲示板ログイン画面
				</h2>
			</div>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						・<c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>


			<form action="login" method="post">
				<br /> <input name="id" value="${User.id}" id="id" type="hidden" />
				<label for="login_id"><c:out value="ログインID" /></label><input name="login_id" value="${loginUser}" placeholder="ログインID"id="login_id" /> <br /><br />
				<label for="password">パスワード</label><input name="password" type="password" id="password" placeholder="パスワード"/> <br /><br />
				<input type="submit" value="ログイン" /> <br /> <br />
			</form>


		<div class="copyright"><jsp:include page="/txt/copyright.txt" /></div>
</div>
</body>
</html>