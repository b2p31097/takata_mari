<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.Date, java.text.DateFormat" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script src="http://web-designer.cman.jp/freejs/cmanCalendar_v092.js"
	charset="utf-8"></script>
<script src="js/jquery-3.3.1.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<link rel="stylesheet" href="./css/jquery-ui.min.css" type="text/css">
<title>掲示板</title>
</head>
<body>
	<div class="main-contents">
		<div class="profile">
			<div class="name">
				<h2>
					社内掲示板
				</h2>
				<c:out value="${loginUser.name}" />さんこんにちは！
			</div>
		</div>

		<div class="titlebar">
   			<p class="newmessage"><a href="newmessage"><input type="button" value="新規投稿"></a></p>
   			<p class="subject"><c:if test="${loginUser.branch_id ==1&&loginUser.position_id ==1}"><a href="management">
   			<input type="button" value="ユーザー管理"></a></c:if></p>
   			<p class="logout"> <a href="logout"><input type="button" value="ログアウト"></a></p>
		</div>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						・<c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="search">
			<form action="./" method="get">
				<label for="category">カテゴリー</label><input name="category" value="${search.category}" id="category" />
				<p><label for="date">投稿日</label>
				<input name="started_date" type="text" value="${search.start_date}" id="start"> ～
				<input name="closed_date" type="text" value="${search.close_date}" id="close"></p>
				<input type="submit" value="検索する">
			</form>
		</div>
	</div>
	<div class="message">
	<table class="user">
	<tr><th class="a"></th>
    <th class="b"></th></tr>
		<c:forEach items="${messages}" var="message">

  				<tr><td><div class="title">【<c:out value="${message.title}" />】</div></td>
  				<td><div class="title"><div class="date">カテゴリー:【<c:out value="${message.category}" />】</div></div></td></tr>

  				<tr><td><div class="date">投稿者:<c:out value="${message.name}" /></div></td>
  				<td><div class="date">投稿日時:
  				<fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div></td></tr>
  				<tr><td colspan="2"><pre><span class="massagetext"><c:out value="${message.text}" /></span></pre>
  					<c:if test="${message.user_id==loginUser.id}">
						<form action="./" method="post">
							<input name="message_id" value="${message.id}" id="message_id" type="hidden" />
							<input type="submit" value="削除する" onclick="return remind()">
						</form>
					</c:if></td></tr>
  				<tr><td>
				</td></tr>

				<c:forEach items="${comments}" var="comments">
					<div class="comment">
						<c:if test="${message.id==comments.message_id}">

							<tr><td><pre><span class="text"><c:out value="${comments.text}" /></span></pre></td></tr>
							<tr><td><span class="name">コメント投稿者
								<c:out value="${comments.name}" /></span></td>
									<td><div class="date">
										投稿日時:<fmt:formatDate value="${comments.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />


									</div></td></tr>

							<tr><td><div class="delete">

								<c:if test="${loginUser.position_id==2||comments.user_id==loginUser.id}">
								<form action="./" method="post">
										<input name="comment_id" value="${comments.id}" id="comment_id" type="hidden" />
										<input type="submit" value="削除する" onclick="return remind()">
									</form>
								</c:if>
							</div></td></tr>
						</c:if>
					</div>
				</c:forEach>
			<tr><td>
			<c:if test="${message.id==comment.message_id}">
				<c:if test="${ not empty commenterrorMessages }">
					<div class="commenterrorMessages">
						<ul>
							<c:forEach items="${commenterrorMessages}" var="commenterrormessage">
								・<c:out value="${commenterrormessage}" />
							</c:forEach>
						</ul>
					</div>
				<c:remove var="commenterrorMessages" scope="session" />
				</c:if>
			</c:if>
			<form action="./" method="post">
				コメント<br />
				<textarea name="comment" cols="100" rows="5" class="text"id="comment"><c:if test="${message.id==comment.message_id}"><c:out value="${comment.text}" /></c:if></textarea>（500文字まで）
				<input name="message" value="${message.id}" id="message" type="hidden" />
				<br /> <input type="submit" value="コメントする">
			</form>
				</td></tr>
		</c:forEach>
	</table>
	</div>

	<div class="copyright"><jsp:include page="/txt/copyright.txt" /></div>
	<script type="text/javascript">
		function remind() {
			myRet = confirm("本当に削除してよろしいですか？");
			if (myRet) {
				alert("削除しました。");
				return true;
			} else {
				alert("キャンセルしました。");
				return false;
			}
		}
		$(function() {
			$("#start").datepicker();
		});
		$(function() {
			$("#close").datepicker();
		});
	</script>
</body>
</html>

