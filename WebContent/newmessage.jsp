<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="./css/style.css" rel="stylesheet" type="text/css">
<title>新規投稿</title>
</head>
<body>
<div class="main-contents">
<div class="profile">
		<div class="name">
			<h2>
				新規投稿画面
			</h2>
		</div>
	</div>
<div class="form-area">
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
		<ul>
		<c:forEach items="${errorMessages}" var="message">
			<li><c:out value="${message}" />
		</c:forEach>
		</ul>
		</div>
			<c:remove var="errorMessages" scope="session" />
	</c:if>
	<form action="newmessage" method="post">
		<label for="title">件名（30文字まで）</label>
		<input type="text"size="55" name="title" value="${message.title}"id="title" /><br /><br />
		<label for="text">本文（1000文字まで）</label>
		<textarea name="text" cols="120" rows="10" id="text"><c:out value="${message.text}" /></textarea><br /><br />
		<label for="category">カテゴリー（10文字まで）</label>
		<input name="category" value="${message.category}" /><br />
		<br /> <input type="submit" value="投稿する">

		<br /><br /><button type="button" onclick="history.back()">戻る</button>

		<div class="copyright"><jsp:include page="/txt/copyright.txt" /></div>

	</form>
</div>
</div>
</body>
</html>