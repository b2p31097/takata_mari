<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー管理画面</title>
</head>
 <body>
 <div class="main-contents">
 	<div class="profile">
		<div class="name">
			<h2>
				管理画面
			</h2>
		</div>
	</div>


	<div class="titlebar">
   		<p class="newmessage"><a href="signup"><input type="button" value="新規登録"></a></p>
  		<p class="subject"><a href="./"><input type="button" value="ホームに戻る"></a></p>
		<p class="logout"> <a href="logout"><input type="button" value="ログアウト"></a></p>
	</div>
	<div class="Users">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<table class="users">
		<tr><th>名前</th><th>ログインID</th><th>支店</th><th>部署・役職</th><th>ログイン</th><th>編集</th><th>ログイン許可</th></tr>
			<c:forEach items="${Users}" var="Users">
  				<tr><td><c:out value="${Users.name}" /></td><td><c:out value="${Users.login_id}" /></td><td><c:out value="${Users.branch_name}" /></td>
  				<td><c:out value="${Users.position_name}" /></td>
  				<td><c:if test="${Users.is_stopped==1}"><span class="codeGreen">許可中</span></c:if>
  				<c:if test="${Users.is_stopped!=1}"><span class="codeRed">停止中</span></c:if></td>
  				<td><form action="setting" method="get">
  				<input name="user_id" value="${Users.id}" id="user_id"type="hidden" /> <br />
  				<input type="submit" value="編集する"></form></td>
  				<td>
  			<c:if test="${Users.is_stopped==1&&loginUser.id!=Users.id}">
				<div class="stop">
					<form action="management" method="post">

						<input name="user_id" value="${Users.id}" id="User_id"type="hidden" /> <br />
						<input type="submit" value="停止する"onclick="return remind()">
					</form>
				</div>
  			</c:if>
 			<c:if test="${loginUser.id==Users.id}">
				<span class="codeGreen">ログイン中</span>
			</c:if>
	  		<c:if test="${Users.is_stopped!=1&&loginUser.id!=Users.id}">
				<div class="stop">
					<form action="management" method="post">
						<input name="user_id" value="${Users.id}" id="User_id"type="hidden" /> <br />
						<input type="submit" value="復活する"onclick="return remind()">
					</form>
				</div>
			</c:if></td>
  </tr>

		</c:forEach>
		</table>
	</div>
</div>

<div class="copyright"><jsp:include page="/txt/copyright.txt" /></div>

<script type="text/javascript">
function remind(){
	myRet = confirm("本当によろしいですか？");
    if ( myRet ){
    	alert("実行しました。");
    	return true;
    }else{
        alert("キャンセルしました。");
        return false;
    }
}
</script>
</body>
</html>