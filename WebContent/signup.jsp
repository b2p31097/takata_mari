<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー登録</title>
</head>
<body>
	<div class="main-contents">
	<div class="profile">
		<div class="name">
			<h2>
				新規登録画面
			</h2>
		</div>
	</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<br />
				<input name="id" value="${User.id}" id="id" type="hidden" />
				<label for="name">名前（10文字まで）</label>
					<input name="name" value="${User.name}"id="name" />

				<br /><label for="login_id">ログインID（半角英数6～20文字まで）</label>
					<input name="login_id" value="${User.login_id}" id="login_id"/>
				<br /><label for="password">パスワード（半角6～20文字まで）</label>
					<input name="password" type="password"id="password" />
				<br /><label for="confirm_password">パスワード(確認)</label>
				<input name="confirm_password" type="password" id="confirm_password" /> <br />
				<label for="branch_id">支店</label>

				<div class="branch">
					<select name="branch_id">
						<c:forEach items="${branch}" var="branch">

							<c:if test="${User.branch_id==branch.id}">
  								<option value="${branch.id}"selected>${branch.name}</option>
							</c:if>
 						<c:if test="${User.branch_id!=branch.id}">
  							<option value="${branch.id}">${branch.name}</option>
						</c:if>
 						</c:forEach>
					</select><br />
					<input name="branch_id"value="${User.branch_id}" id="branch_id"type="hidden" />
				</div>

				<label for="position_id">部署・役職</label>
				<select name="position_id">
						<c:forEach items="${position}" var="position">
							<c:if test="${User.position_id==position.id}">
  								<option value="${position.id}"selected>${position.name}</option>
							</c:if>
 							<c:if test="${User.position_id!=position.id}">
  								<option value="${position.id}">${position.name}</option>
							</c:if>
 						</c:forEach>
				</select><br/><br/>
					<input name="position_id" value="${User.position_id}" id="position_id"type="hidden">
					<input type="submit" value="登録" /> <br /><br />

			<button type="button" onclick="history.back()">戻る</button>

		</form>

		<div class="copyright"><jsp:include page="/txt/copyright.txt" /></div>
	</div>
</body>
</html>
