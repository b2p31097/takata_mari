<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${editUser.login_id}の設定</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="profile">
			<div class="name">
				<h2>
					編集画面
				</h2>
			</div>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						・<c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

	<form action="setting" method="post">
		<br /> <input name="id" value="${editUser.id}" id="id" type="hidden" />
		<label for="name">名前（10文字まで）</label><input name="name"value="${editUser.name}" id="name" /><br />
		<label for="login_id">ログインID（半角英数6～20文字まで）</label><input name="login_id"value="${editUser.login_id}" /><br />
		<label for="password">パスワード（半角6～20文字まで）</label><input name="password" type="password" id="password" /> <br />
		<label for="confirm_password">パスワード(確認)</label><input name="confirm_password" type="password" id="confirm_password" /> <br />
		<label for="branch_id">支店</label>

			<c:if test="${loginUser.id!=editUser.id}">
				<div class="branch">
					<select name="branch_id">
						<c:forEach items="${branch}" var="branch">

							<c:if test="${editUser.branch_id==branch.id}">
  								<option value="${branch.id}"selected>${branch.name}</option>
							</c:if>
 						<c:if test="${editUser.branch_id!=branch.id}">
  							<option value="${branch.id}">${branch.name}</option>
						</c:if>
 						</c:forEach>
					</select><br />
					<input name="branch_id"value="${editUser.branch_id}" id="branch_id"type="hidden" />
				</div>
  			</c:if>

				<c:if test="${loginUser.id==editUser.id}">
				<c:forEach items="${branch}" var="branch">
				<c:if test="${editUser.branch_id==branch.id}">
  					<c:out value="${branch.name}" />
				</c:if>
				</c:forEach>
  			<div class="info">本人のため編集できません</div>
  			<input name="branch_id"value="${editUser.branch_id}" id="branch_id"type="hidden" />
			</c:if>

			<label for="position_id">部署・役職</label>
				<c:if test="${loginUser.id!=editUser.id}">
					<select name="position_id">
						<c:forEach items="${position}" var="position">
							<c:if test="${editUser.position_id==position.id}">
  								<option value="${position.id}"selected>${position.name}</option>
							</c:if>
 							<c:if test="${editUser.position_id!=position.id}">
  								<option value="${position.id}">${position.name}</option>
							</c:if>
 						</c:forEach>
					</select>
				</c:if>
			<input name="position_id" value="${editUser.position_id}" id="position_id"type="hidden">


  			<c:if test="${loginUser.id==editUser.id}">
  				<c:forEach items="${position}" var="position">
  					<c:if test="${editUser.position_id==position.id}"><c:out value="${position.name}" /></c:if>
  				</c:forEach>
  				<div class="info">本人のため編集できません</div>
  				<input name="position_id" value="${editUser.position_id}" id="position_id"type="hidden">
			</c:if>

			<br /><br /><input type="submit" value="登録" /> <br /><br />
			<button type="button" onclick="history.back()">戻る</button>

	</form>

<div class="copyright"><jsp:include page="/txt/copyright.txt" /></div>
</div>
</body>
</html>